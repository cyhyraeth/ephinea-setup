# Ephinea Setup

1. [Community](#community)
2. [Gamepad](#gamepad)
3. [Addons](#addons)
4. [Skins](#skins)
5. [Tools](#tools)
6. [Tips](#tips)
7. [Mags](#mags)
8. [Guides](#guides)

<!-- have a list of links to each section at the top -->

## Community

### Discord

https://discord.gg/pso

### Forums

https://www.pioneer2.net/community/#ephinea-pso-bb-private-server.18

## Gamepad

<!-- include screenshots of example configuration -->

<!-- explain how to map multiple layers, chat
      shortcuts, action bar, triggers, turbo -->

https://github.com/AntiMicroX/antimicrox

## Addons

https://www.pioneer2.net/community/threads/psobb-addon-plugin-lua-ui-addons.4543/

<!-- include screenshots of example setup -->

## Skins

<!-- include screenshots of example setup -->

https://www.pioneer2.net/community/threads/echelons-skins-modifications.4357/

## Tools

<!-- explain how to unbind back button with input map -->

https://www.pioneer2.net/community/threads/solys-stash.1001/

## Tips

### Materials

<!-- recommend using min-max material plan for your class -->

<!-- talk about checking and resetting materials, except hp -->

https://wiki.pioneer2.net/w/Class_guides

### Section ID

<!-- can change Section ID before level 20 -->

https://wiki.pioneer2.net/w/Section_IDs

### Drop Charts

https://ephinea.pioneer2.net/drop-charts/ultimate/

## Mags

https://wiki.pioneer2.net/w/Mags

https://wiki.pioneer2.net/w/Mags/Guide

https://wiki.pioneer2.net/w/Mag_feeding_tables

## Guides

### Client Setup

https://www.pioneer2.net/community/threads/quick-client-setup-guide.16052/

### Anti-Dark Ring

https://www.pioneer2.net/community/threads/te-adr-guide.21773/
